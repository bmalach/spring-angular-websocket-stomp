import { Component, OnInit } from '@angular/core';
import {RxStompService} from "../rx-stomp.service";
import {Message} from "../payload/Message";

@Component({
  selector: 'app-messages',
  templateUrl: './messages.component.html',
  styleUrls: ['./messages.component.css']
})
export class MessagesComponent implements OnInit {

  constructor(
    private rxStompService: RxStompService
  ) { }

  ngOnInit(): void {
    this.rxStompService.watch('/topic/one').subscribe(msg => {
      console.log(msg);
    });
    this.rxStompService.watch('/topic/xd').subscribe(msg => {
      console.log(msg);
    });
  }

  onSendMessage() {
    console.log('click')
    const msg = {
      firstName: 'Johhny',
      lastName: `${new Date()}`
    } as Message;

    this.rxStompService.publish({
      destination: '/app/hello',
      body: JSON.stringify(msg)
    });

  }

  onSendMessage2() {
    console.log('click2')
    const msg = {
      firstName: 'Johhny',
      lastName: `${new Date()}`
    } as Message;

    this.rxStompService.publish({
      destination: '/app/hello2',
      body: JSON.stringify(msg)
    });

  }

}
