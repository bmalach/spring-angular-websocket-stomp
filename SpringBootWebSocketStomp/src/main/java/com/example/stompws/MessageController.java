package com.example.stompws;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.messaging.MessageHeaders;
import org.springframework.messaging.handler.annotation.MessageMapping;
import org.springframework.messaging.handler.annotation.SendTo;
import org.springframework.messaging.simp.SimpMessageHeaderAccessor;
import org.springframework.messaging.simp.SimpMessagingTemplate;
import org.springframework.stereotype.Controller;

import javax.servlet.http.HttpSession;
import java.security.Principal;

@Controller
public class MessageController {

    private final SimpMessagingTemplate template;

    @Autowired
    public MessageController(SimpMessagingTemplate template) {
        this.template = template;
    }

    @MessageMapping("/hello")
    @SendTo("/topic/one")
    public MessageResponse one(Message msg) throws InterruptedException {
        System.out.println("/hello");
        Thread.sleep(3000);
        return new MessageResponse(String.format("%s %s", msg.getFirstName(), msg.getLastName()));
    }

    @MessageMapping("/hello2")
    public void two(Message msg, MessageHeaders headers, SimpMessageHeaderAccessor headerAccessor, Principal principal) throws InterruptedException {
        System.out.println("/hello2");
//        System.out.println(headers.getId());
//        System.out.println(headerAccessor.getSubscriptionId());
        System.out.println(headerAccessor.getSessionId());
//        Thread.sleep(3000);
        System.out.println(headerAccessor);
        System.out.println(headers);
        headerAccessor.getSessionAttributes().put("xd","XDXD");
//        System.out.println(principal.getName());
        System.out.println(msg.getLastName());
        template.convertAndSend("/topic/xd", "----------> ");
    }


}
